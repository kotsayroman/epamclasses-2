using System;

namespace Epam.Tasks.Vector
{	
	class Vector : IComparable
	{
		public Vector()
		{
			X = 0;
			Y = 0;
			Z = 0;
		}
		public Vector(double x, double y, double z)
		{
			X = x;
			Y = y;
			Z = z;
		}
		
		public double X { get; private set; }
		public double Y { get; private set; }
		public double Z { get; private set; }
		
		public double Magnitude
		{
			get
			{
				return Math.Sqrt(X * X + Y * Y + Z * Z);
			}
		}
		
		public override bool Equals(object obj)
		{			
			Vector other = obj as Vector;
			
			if (obj == null)
			{
				return false;
			}
			else
			{
				return (X == other.X) &&
				(Y == other.Y) &&
				(Z == other.Z);
			}			
		}
		
		public override int GetHashCode()
		{
			int hash = 17;
			
			hash = 23 * hash + X.GetHashCode();
			hash = 23 * hash + Y.GetHashCode();
			hash = 23 * hash + Z.GetHashCode();
			
			return hash;
		}
		
		public override string ToString()
		{
			return String.Concat("(", X.ToString(), "; ", Y.ToString(), "; ", Z.ToString(), ";)");
		}
		
		public int CompareTo(object obj)
		{
			Vector other = obj as Vector;
			
			if (other == null || Magnitude > other.Magnitude)
			{
				return 1;
			}
			else if (this.Magnitude < other.Magnitude)
			{
				return -1;
			}
			else
			{
				return 0;
			}
		}	
		
		public void Add(Vector other)
		{
			X += other.X;
			Y += other.Y;
			Z += other.Z;
		}	
		
		public void Subtract(Vector other)
		{
			X -= other.X;
			Y -= other.Y;
			Z -= other.Z;
		}
		
		public void VectorProduct(Vector other)
		{
			X = Y * other.Z - Z * other.Y;
			Y = Z * other.X - X * other.Z;
			Z = X * other.Y - Y * other.X;
		}
		
		public double ScalarProduct(Vector other)
		{
			return X * other.X + Y * other.Y + Z * other.Z;
		}
		
		public double TripleProduct(Vector second, Vector third)
		{
			return X * (second.Y * third.Z - second.Z * third.Y) - 
			Y * (second.X * third.Z - second.Z * third.X) -
			Z * (second.X * third.Y - second.Y * third.X);
		}
		
		public double AngleBetweenVectors(Vector other)
		{
			if ((Magnitude == 0) || (other.Magnitude == 0))
			{
				return 0;			
			}
			else
			{
				double cosFi = ScalarProduct(other) / (Magnitude * other.Magnitude);
				return Math.Acos(cosFi) * 180 / Math.PI;
			}
		}
		
		public static Vector Add(Vector first, Vector second)
		{
			return new Vector(first.X + second.X, first.Y + second.Y, first.Z + second.Z);
		}	
		
		public static Vector Subtract(Vector first, Vector second)
		{
			return new Vector(first.X - second.X, first.Y - second.Y, first.Z - second.Z);
		}

		public static Vector VectorProduct(Vector first, Vector second)
		{
			return new Vector(first.Y * second.Z - first.Z * second.Y, first.Z * second.X - first.X * second.Z, first.X * second.Y - first.Y * second.X);
		}
		
		public static double ScalarProduct(Vector first, Vector second)
		{
			return first.X * second.X + first.Y * second.Y + first.Z * second.Z;
		}
					
		public static double TripleProduct(Vector first, Vector second, Vector third)
		{
			return first.X * (second.Y * third.Z - second.Z * third.Y) - 
			first.Y * (second.X * third.Z - second.Z * third.X) - 
			first.Z * (second.X * third.Y - second.Y * third.X);
		}
		
		public static double AngleBetweenVectors(Vector first, Vector second)
		{
			if ((first.Magnitude == 0) || (second.Magnitude == 0))
			{
				return 0;			
			}
			else
			{
				double cosFi = ScalarProduct(first, second) / (first.Magnitude * second.Magnitude);
				return Math.Acos(cosFi) * 180 / Math.PI;
			}
		}	
				
		public static bool operator ==(Vector first, Vector second)
		{
			return first.Equals(second);
		}
		
		public static bool operator !=(Vector first, Vector second)
		{
			return !first.Equals(second);
		}
		
		public static bool operator >(Vector first, Vector second)
		{
			return (first.CompareTo(second) > 0);
		}
		
		public static bool operator <(Vector first, Vector second)
		{
			return (first.CompareTo(second) < 0);
		}
		
		public static bool operator >=(Vector first, Vector second)
		{
			return (first.CompareTo(second) >= 0);
		}
		
		public static bool operator <=(Vector first, Vector second)
		{
			return (first.CompareTo(second) <= 0);
		}
		
		public static Vector operator +(Vector first, Vector second)
		{
			return Add(first, second);
		}
		
		public static Vector operator -(Vector first, Vector second)
		{
			return Subtract(first, second);
		}
		
		public static Vector operator *(Vector first, Vector second)
		{
			return VectorProduct(first, second);
		}
	}
	
	class RepresentingVectors
	{
		static void Main()
		{
			// Creating new vectors
			var a = new Vector(1, 1, 1);
			var b = new Vector(1, 2, 3);
			var c = new Vector(-1, 0, 5.58);
			var d = new Vector(1, 1, 1);
			Console.WriteLine("Vector a: {0}", a);
			Console.WriteLine("Vector b: {0}", b);
			Console.WriteLine("Vector c: {0}", c);
			Console.WriteLine("Vector d: {0}", d);
			Console.WriteLine();
			
			// Adding and subtracting vectors
			Vector aPlusB = a + b;
			Vector aMinusB = a - b;
			Console.WriteLine("{0} plus {1} equals: {2}", a, b, aPlusB);
			Console.WriteLine("{0} minus {1} equals: {2}", a, b, aPlusB);
			
			aPlusB = Vector.Add(a, b);
			aMinusB = Vector.Subtract(a, b);
			Console.WriteLine("{0} plus {1} equals: {2}", a, b, aPlusB);
			Console.WriteLine("{0} minus {1} equals: {2}", a, b, aPlusB);
			
			Console.Write("{0} plus {1} equals: ", a, b);
			a.Add(b);
			Console.WriteLine(a);
			
			Console.Write("{0} minus {1} equals: ", a, b);
			a.Subtract(b);			
			Console.WriteLine(a);
			Console.WriteLine();
			
			// Calculating the scalar product of vectors			
			double scalarProductOfAB = Vector.ScalarProduct(a, b);
			Console.WriteLine("The scalar product of {0} and {1} equals: {2}", a, b, scalarProductOfAB);
			
			scalarProductOfAB = a.ScalarProduct(b);
			Console.WriteLine("The scalar product of {0} and {1} equals: {2}", a, b, scalarProductOfAB);
			Console.WriteLine();
			a = new Vector(1, 1, 1);
			
			// Calculating the vector product of vectors
			Vector vectorProductOfAB = a * b;
			Console.WriteLine("The vector product of {0} and {1} equals: {2}", a, b, vectorProductOfAB);
			
			vectorProductOfAB = Vector.VectorProduct(a, b);
			Console.WriteLine("The vector product of {0} and {1} equals: {2}", a, b, vectorProductOfAB);
			
			Console.WriteLine("The vector product of {0} and {1} equals:", a, b);
			a.VectorProduct(b);
			Console.WriteLine(a);
			Console.WriteLine();
			a = new Vector(1, 1, 1);
			
			// Calculating the triple product of vectors
			double tripleProductOfABC = Vector.TripleProduct(a, b, c);
			Console.WriteLine("The triple product of a, b and c equals: {0}", tripleProductOfABC);
			
			tripleProductOfABC = a.TripleProduct(b, c);
			Console.WriteLine("The triple product of a, b and c equals: {0}", tripleProductOfABC);
			Console.WriteLine();
						
			// Calculating vector`s magnitude
			double aMagnitude = a.Magnitude;
			double bMagnitude = b.Magnitude;
			Console.WriteLine("The magnitude of vector {0} equals: {1}", a, aMagnitude.ToString("F3"));
			Console.WriteLine("The magnitude of vector {0} equals: {1}", b, bMagnitude.ToString("F3"));
			Console.WriteLine();
			
			// Calculating the angle between two vectors
			double angleBetweenAandB = Vector.AngleBetweenVectors(a, b);
			Console.WriteLine("The angle between {0} and {1} equals: {2} degrees", a, b, angleBetweenAandB.ToString("F6"));
			
			angleBetweenAandB = a.AngleBetweenVectors(b);
			Console.WriteLine("The angle between {0} and {1} equals: {2} degrees", a, b, angleBetweenAandB.ToString("F6"));
			Console.WriteLine();	
			
			// Comparing vectors
			bool aEqualsD = (a == d);
			bool aEqualsC = (a != c);
			Console.WriteLine("{0} equals {1}: {2}", a, d, aEqualsD);
			Console.WriteLine("{0} doesn`t equal {1}: {2}", a, c, aEqualsC);
			
			aEqualsD = a.Equals(d);
			Console.WriteLine("{0} equals {1}: {2}", a, d, aEqualsD);
			
			bool aGreaterThanD = (a > d);
			bool aGreaterThanOrEqualToD = (a >= d);
			bool aLessThanС = (a < c);
			
			Console.WriteLine("{0} is greater than {1}: {2}", a, d, aGreaterThanD);
			Console.WriteLine("{0} is greater than or equal to {1}: {2}", a, d, aGreaterThanOrEqualToD);
			Console.WriteLine("{0} is less than {1}: {2}", a, c, aLessThanС);
			Console.WriteLine();		
		}
	}
}